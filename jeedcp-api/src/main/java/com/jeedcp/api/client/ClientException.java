package com.jeedcp.api.client;

public class ClientException extends RuntimeException {

	private static final long serialVersionUID = -1108392076700488161L;

	public ClientException(String message) {
		super(message);
	}

}
