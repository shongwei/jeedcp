package com.jeedcp.api.test;


public class SearchStudentEntity  {
	private Integer schId;
	private String schName;
	private Integer schPoliticsStatus;
	private String schNationality;
	private String schStuNo;

	public Integer getSchId() {
		return schId;
	}

	public void setSchId(Integer schId) {
		this.schId = schId;
	}

	public String getSchName() {
		return schName;
	}

	public void setSchName(String schName) {
		this.schName = schName;
	}

	public Integer getSchPoliticsStatus() {
		return schPoliticsStatus;
	}

	public void setSchPoliticsStatus(Integer schPoliticsStatus) {
		this.schPoliticsStatus = schPoliticsStatus;
	}

	public String getSchNationality() {
		return schNationality;
	}

	public void setSchNationality(String schNationality) {
		this.schNationality = schNationality;
	}

	public String getSchStuNo() {
		return schStuNo;
	}

	public void setSchStuNo(String schStuNo) {
		this.schStuNo = schStuNo;
	}

}
