package com.jeedcp.api.message;

public interface Error {
	String getCode();
    String getMessage();
}
