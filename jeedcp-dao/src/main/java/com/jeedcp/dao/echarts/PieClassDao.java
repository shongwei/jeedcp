/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeedcp.org/">Jeedcp</a> All rights reserved.
 */
package com.jeedcp.dao.echarts;


import com.jeedcp.dao.base.CrudDao;
import com.jeedcp.entity.echarts.PieClass;
import com.jeedcp.persistence.annotation.MyBatisDao;

/**
 * 班级DAO接口
 * @author lgf
 * @version 2016-05-26
 */
@MyBatisDao
public interface PieClassDao extends CrudDao<PieClass> {

	
}