/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeedcp.org/">jeedcp</a> All rights reserved.
 */
package com.jeedcp.common.beanvalidator;

/**
 * 默认Bean验证组
 * @author jeedcp
 */
public interface DefaultGroup {

}
